FROM maven:3.5-jdk-8-alpine as build
WORKDIR /atlassian
COPY atlassian /atlassian
RUN mvn install